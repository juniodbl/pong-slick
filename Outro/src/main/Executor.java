package main;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;

public class Executor 
{
	public static void main( String[] args )  
	{
		try
		{
			AppGameContainer appgc = new AppGameContainer(new Mundo("Simple Slick Game"));
			appgc.setDisplayMode(640, 480, false);
			appgc.setShowFPS(false);
			appgc.start();
		}
		catch (SlickException ex)
		{
			Logger.getLogger(Mundo.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}