package main;

import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.command.BasicCommand;
import org.newdawn.slick.command.Command;
import org.newdawn.slick.command.InputProvider;
import org.newdawn.slick.command.InputProviderListener;
import org.newdawn.slick.command.KeyControl;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Rectangle;

/**
 * Pong Game utilizando o framework Slick2d.
 * 
 * Todos os direitos s�o reservados au autor e aos desenvolvedores da <a href='http://slick.ninjacave.com'>Slick2D</a><br/>
 * Site do autor: <a href='https://erro23.blogspot.com'>https://erro23.blogspot.com</a> 
 * 
 * @author juniodbl 
 * @version 1.0.0.0
 */
/**
 * @author mauricio_lourenco
 *
 */
public class Mundo extends BasicGame
{
	/*
	 * Representa��es dos objetos tulizados no jogo
	 */
	Rectangle jogador, inimigo;
	Circle bola;
	
	/*
	 * Velocidade da bola dentro do jogo, cada um controla a velocidade dentro de um eixo 
	 */
	float velocidadeX = 1.0f;
	float velocidadeY = 0.4f;
	
	/*
	 * velocidade do jogador, ele se move apenas dentro de um eixo, o Y
	 */
	float velocidadeJogador = 1.0f;
	
	/*
	 * velocidade do inimimgo, ele se move apenas dentro de um eixo, o Y
	 */
	float velocidadeInimigo = 0.4f;
	
	/*
	 * Label utilizada para colocar a pontua��ao na tela
	 */
	String labelPontosJogador = "Jogodor: ";
	String labelPontosInimigo = "Inimigo: ";
	
	/*
	 * Controlam a pontua��o do jogador e do inimigo
	 */
	int pontoJogador = 0;
	int pontoInimigo = 0;
	
	/*
	 * esta variavel controla se a bola esta parada ou n�o, funciona como um pause, 
	 * mas deixa o jogador livre para se movimentar
	 */
	boolean startBola = false;
	
	/*
	 * Est� lista controla as teclas que est�o precionadas no momento.
	 */
	List<BasicCommand> comandosPresionados = new ArrayList<>( );

	/*
	 * Fonte da desenhar na tela, aqui utilizo el� apenas para 
	 * calcular o tamanho de uma string em pixels 
	 */
	TrueTypeFont m_font;
	
	/**
	 * Contrutor padr�o de um {@link BasicGame}
	 * @param title
	 */
	public Mundo( String title )
	{
		super(title);
	}

	/**
	 * Ee met� � aonde devemos fazer o carregamento de nosso conteudo, como imagens, 
	 * fontes, entre outros tipos de arquvos ou dados.
	 * @param container
	 * @throws SlickException
	 */
	@Override
	public void init(GameContainer container) throws SlickException
	{
		/*
		 * inicializa��o do jogador, inimigo e bola, definimos seu tamanho e sua posi��o na tela.
		 * 
		 * tambem fazemos a defini��o dos eventos neste metodo, mas apenas delegando sua chamada para outro.
		 * 
		 * OBS: o plano cartesiano que a Slick2D utiliza, � com o ponto (x=0,y=0) 
		 * no canto superior esquerdo, e todos os desenhos feitos na tela, tambem
		 * s�o iniciados no cantos superiror esquerdo.
		 */
		jogador = new Rectangle(10, 10, 20, 100);
		inimigo = new Rectangle(container.getWidth()-30, 10, 20, 100);
		m_font = new TrueTypeFont( new Font( "Arial", Font.PLAIN, 20 ), false );
		criarBola(container);
		criarEventos( container );
	}
	
	/**
	 * aqui fazemos a cra��o da bola, toda vez que este metodo for chamado, ele recria a bola e 
	 * coloca ela no meio da tela parada.
	 * @param container
	 */
	private void criarBola( GameContainer container )
	{
		bola = new Circle(container.getWidth()/2, container.getHeight()/2, 20);
		startBola = false;
	}

	/**
	 * Aqui fazemos a defini��o dos eventos, apenas utilizamos teclado neste jogo.
	 * 
	 * @param container
	 */
	private void criarEventos( GameContainer container )
	{
		InputProvider ip = new  InputProvider(container.getInput());//obtemos o provider do teclado

		/*
		 * aqui fazenmos um bind entre uma tecla e um commando, quando uma tecla 'X' for acionada
		 * ir� disparar um comando que sera escutado pela classe que criamos 
		 */
		ip.bindCommand( new KeyControl( Input.KEY_SPACE ), new BasicCommand( "start" ) );
		ip.bindCommand( new KeyControl( Input.KEY_UP ), new BasicCommand( "up" ) );
		ip.bindCommand( new KeyControl( Input.KEY_DOWN ), new BasicCommand( "down" ) );
		
		/*
		 * dicionamos uma classe interna para escutar os eventos de quando um comando for acionado
		 */
		ip.addListener(new InputProviderListener( )
		{
			@Override
			public void controlReleased(Command command) 
			{
				/*
				 * aqui quando um comando for solto removemos da lista de teclas precionados
				 */
				comandosPresionados.remove(command);
			}
			
			@Override
			public void controlPressed(Command command) 
			{
				BasicCommand basicCommand = (BasicCommand)command;
				if( basicCommand.getName( ).equals( "start" ) )//aqui caso o cammando acionado seja o start invertemos o estado da bola
				{
					startBola = !startBola;
				}
				else if( basicCommand.getName( ).equals( "up" ) ||
						basicCommand.getName( ).equals( "down" ) )
				{
					comandosPresionados.add(basicCommand);//adicionamos o cammando na lista 
				}
			}
		});
	}

	/**
	 * Aqui acontece toda a atualiza��o da logica do jogo.
	 * 
	 * @param container
	 * @param delta
	 * @throws SlickException
	 */
	@Override
	public void update(GameContainer container, int delta) throws SlickException
	{
		if( startBola == true )
		{
			atualizaBola( container, delta );//atualizaremos o estado da bola caso possivel
		}
		
		if( !comandosPresionados.isEmpty( ) )
		{
			atualizaJogador( container, delta );//atualizaremos o estado do jogador caso tenha alguma tecla precionada
		}
		
		atualizaInimigo( container, delta );//atualizaremos o estado do inimigo
	}

	/**
	 * aqui validamos se o inimigo est� descendo ou sumindo e 
	 * atualizamos sua posi��o em rela��o a bola
	 * @param container
	 * @param delta
	 */
	private void atualizaInimigo( GameContainer container, float delta )
	{
		if( inimigo.getY()-bola.getY() < 0 &&
			inimigo.getHeight( ) + inimigo.getY( ) < container.getHeight( ) )//descendo
		{
			inimigo.setY( inimigo.getY( ) + (velocidadeInimigo * delta) );
		}
		
		if( inimigo.getY()-bola.getY() > 0 &&
		    inimigo.getY( ) > 0 )//subindo
		{
			inimigo.setY( inimigo.getY( ) - (velocidadeInimigo * delta) );
		}
	}
	
	/**
	 * aqui validamos se o jogador est� descendo ou sumindo e 
	 * atualizamos sua posi��o de acordo com os mcliques do teclado
	 * @param container
	 * @param delta
	 */
	private void atualizaJogador( GameContainer container, float delta )
	{
		if( comandosPresionados.get(0).getName().equals("down") && 
			jogador.getHeight( ) + jogador.getY( ) < container.getHeight( ) )
		{
			jogador.setY(jogador.getY()+(velocidadeJogador*delta));
		}
		
		if( comandosPresionados.get(0).getName().equals("up") &&
			jogador.getY( ) > 0 )
		{
			jogador.setY(jogador.getY()-(velocidadeJogador*delta));
		}
	}

	/**
	 * atualizamos o estado da bola
	 * @param container
	 * @param delta
	 */
	private void atualizaBola( GameContainer container, float delta )
	{
		/*
		 * verifica se passou do limite da tela do inimigo, da um ponto para o jogador e recia a bola
		 */
		if( bola.getWidth( ) + bola.getX( ) >= container.getWidth( ) )
		{
			 pontoJogador++;
			 criarBola(container);
		}
		
		/*
		 * verifica se passou do limite da tela do jogador, da um ponto para o inimigo e recia a bola
		 */
		if( bola.getX( ) <= 0 )
		{
			pontoInimigo++;
			criarBola(container);
		}
		
		/*
		 * inverte a dire��o do eixo y caso ela rebata na parte superior ou inferior da tela
		 */
		if( bola.getHeight( ) + bola.getY( ) >= container.getHeight( ) || 
			bola.getY( ) <= 0 )
		{
			velocidadeY = velocidadeY * -1; 
		}

		/*
		 * inverte a dire��o do eixo x caso ela bata no inimigo, e recoloca ela um pouco mas a 
		 * frete dele quando rebatida
		 */
		if( bola.intersects( inimigo ) ) 
		{
			bola.setX( inimigo.getX( ) - bola.getWidth( ) - 1 );
			velocidadeX = velocidadeX * -1;
		}
		
		/*
		 * inverte a dire��o do eixo x caso ela bata no jogador, e recoloca ela um pouco mas a 
		 * frete dele quando rebatida
		 */
		if( bola.intersects( jogador ) )
		{
			bola.setX( jogador.getX( ) + jogador.getWidth( ) + 1 );
			velocidadeX = velocidadeX * -1;
		}
		
		//atualiza a posi��o da bola na tela
		bola.setX(bola.getX()+(velocidadeX*delta));
		bola.setY(bola.getY()+(velocidadeY*delta));
	}
	
	/**
	 * Aqui � aonde fazemos os desenhos na tela
	 * 
	 * @param container
	 * @param g
	 * @throws SlickException
	 */
	@Override
	public void render(GameContainer container, Graphics g) throws SlickException 
	{
		/*
		 * desenha o jogador
		 */
		g.setColor(Color.red);
		g.draw(jogador);
		
		/*
		 * desenha o inimigo
		 */
		g.setColor(Color.blue);
		g.draw(inimigo);
		
		/*
		 * desenha a bola
		 */
		g.setColor(Color.green);
		g.draw(bola);
		
		/*
		 * desenha os pontos do jogador
		 */		
		g.setColor(Color.white);
		g.drawString(labelPontosJogador+pontoJogador, 10, 10);
		
		/*
		 * desenha os pontos do inimigo, e para que n fique escondido na tela, fazemos um
		 * calculo com o tamanha da string em pixels.
		 */	
		String label = labelPontosInimigo+pontoInimigo;
		g.drawString(label, container.getWidth()-10-(m_font.getWidth(label)), 10);
	}
}